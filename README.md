# Hexapawn

### Une version naive du programme

Cette version Naive et la version Dynamique aurait du etre composer de seulment 3 objets:

- Un main qui aurait ochestrer notre 'jeu'
- Une class Computer qui aurait servie de classe de calcule
- Une classe globale qui sert d'initialiseur et de stockage de valeur globale

Le dérouler de la version naive est un appelle récursif sur chaques possibilitées et calcule 
donc toutes les configurations possible.  

Ici nous avons les deux fonctions qui servent dans l'appele recursif. 

A savoir 
```py
        def algo(self, plateau: Plateau, tour: int = 0) -> int:
        joueurActu = (tour % 2) - 1 if tour % 2 == 0 else (tour % 2)
        listVal: [int] = []
        # -1 si 1 | 1 si 0 pour les methodes de jouabilités de Plateau
        if (plateau.aPerdu(joueurActu)):
            r = tour
            if (tour % 2 == 0):
                r = tour * -1
            return r
        else:
            tour += 1
            for l in range(plateau.getNbLigne()):
                for c in range(plateau.getNbColone()):
                    if plateau.getPlateau()[l][c] == joueurActu and plateau.isPionMovable(l, c, joueurActu):
                        listVal.append(self.devAlgo(plateau, tour, joueurActu, l, c))
            return self.getMeilleurCoup(listVal)
```

Où l'on determine le joueur actuel grâce au tour qui est un simple Int qui s'incremente.
On verifie ensuite si le joueur n'a pas perdu, condition de fin d'arret de notre recursivité.
Ensuite on cherche tous ces pions sur le plateau et on appel devAlgo qui rappelera lui-même 
l'algo afin de créer la recursivité. 

On retourne ensuite le meilleur coup possible grâce à la valeur de tous les 
coups possible pour chaques pions (on choisi donc ici le meilleur predecesseur)

Si l'on avait reussi à implementer l'algo Dynamique c'est ici que tout
se serait joué. Ils nous auraient fallu d'enregistrer le plateau avec le meilleur coup.
Se qui nous aurait permis de le retourner avant sans devoir essayer de recalculer la position

```py
    def devAlgo(self, plateau: Plateau, tour: int, joueurActu: int, l: int, c: int) -> int:
        listVal: [int] = []

        platSave = plateau.getCopy()
        if plateau.pionAvance(l, c, joueurActu):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        platSave = plateau.getCopy()
        if plateau.pionTake(l, c, joueurActu, 'd'):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        platSave = plateau.getCopy()
        if plateau.pionTake(l, c, joueurActu, 'g'):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        return self.getMeilleurCoup(listVal)
```
La fonction devAlgo sert à tester tous les coups disponible pour le pion trouver et relance ensuite l'algo avec le nouveau plateau où le coup a été joué

Le probleme de ce code ci est son temps d'execution, a cause du calcule de tout les coups possibles.  
En effet comme déduit avec certain essay de notre pars si on dépasse un plateau de 4 par 3 ou 3 par 4 le nombre de 
configuration devient suffisament grand pour que le temps d'execution se rallonge sinificativement.  
C'est la que la programation dynamique était util car nous *'déplacions'* de la complexiter de calcule dans de la complexiter mémoire.

Si dans le cadre de ce projet nous avions pue le faire nous aurion eu une structure de donné éfficace qui aurait 
permit de stocker chaque configuration avec sa valuation. Cette structure de donné devait etre capable de trouver une donné
rapidement (de préférence en temps constant comme une hashMap).

Nous aurions donc verifier si notre configuration avais était deja calculer, si oui alors nous renvoyon sa valuation et 
sinon on la calculer puis on la renvoie.


Birlouez Martin & Roy Victor
 
