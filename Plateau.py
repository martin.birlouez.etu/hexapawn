import copy

class Plateau:
    # dans le plateau on a les pion blanc qui son -1 et les pion noire qui son 1 le reste est a 0
    __plateau: [[int]] = []
    __nbLignes: int
    __nbColones: int

    def __init__(self, l: int, c: int):
        self.__nbColones = c
        self.__nbLignes = l
        self.initPlateauValues()

    def getCopy(self) -> [[int]]:
        return copy.deepcopy(self.__plateau)

    def initPlateauValues(self):
        for i in range(self.__nbLignes):
            self.__plateau.append([0] * self.__nbColones)

        self.__plateau[0] = [1] * self.__nbColones
        self.__plateau[self.__nbLignes - 1] = [-1] * self.__nbColones

        for i in range(self.__nbColones):  # init des pions
            self.__plateau[0][i] = (0, i)
            self.__plateau[1][i] = (self.__nbLignes - 1, i)

    def setValue(self, l: int, c: int, v: int):
        self.__plateau[l][c] = v

    def getValue(self, l: int, c: int):
        return self.__plateau[l][c]

    def toString(self):
        r = ""
        for l in self.__plateau:
            r += '| '
            for c in l:
                s = 'n' if c == 1 else 'b' if c == -1 else '.'
                r += ' ' + s  + ' |'
            r += '\n'
        return r

    def getNbLigne(self) -> int:
        return self.__nbLignes

    def getNbColone(self) -> int:
        return self.__nbColones

    def getPlateau(self) -> [[int]]:
        return self.__plateau

    def setPlateau(self, plat: [[int]]) -> bool:
        self.__plateau = plat
        return True

    def canPionTake(self, l: int, c: int, j: int , side: str) -> bool:
        r = False
        if side == 'g':
            if c-1 >= 0 :
                if self.__plateau[l + j][c - 1] == (j*-1) :
                    r = True
        elif side == 'd':
            if (c+1) < self.getNbColone() :
                if self.__plateau[l + j][c + 1]== (j*-1) :
                    r = True
        
        return r

    def canPionAvance(self, l: int, c: int, j: int) -> bool:   
        return self.__plateau[l + j][c] == 0

    def isPionMovable(self, l: int, c: int, j: int) -> bool:
        return self.__plateau[l + j][c] == 0 or self.canPionTake(l, c, j, 'd') or self.canPionTake(l, c, j, 'g')

    def pionAvance(self, l: int, c: int, j: int) -> bool:
        r = False
        if self.canPionAvance(l, c, j):
            self.__plateau[l][c] = 0
            self.__plateau[l + j][c] = j
            r = True
            
        return r

    def pionTake(self, l: int, c: int, j: int, side: str) -> bool:
        # side : d=droite g=gauche pas d'invertion pour blanc (pas d'effet miroir pour le joueur de l'autre cote du plateau*)
        r = False

        if self.canPionTake(l, c, j, side):

            if side == 'd':
                if (c+1) < self.getNbColone() :
                    self.__plateau[l][c] = 0
                    self.__plateau[l + j][c + 1] = j
                    r = True
            elif side == 'g':
                if c-1 >= 0 :
                    self.__plateau[l][c] = 0
                    self.__plateau[l + j][c - 1] = j
                    r = True
            
        return r

    def aPerdu(self, j: int) -> bool:
        """
        1 = Blanc
        -1 = Noir
        """

        if (-1 in self.__plateau[0]) and (j==1):
            return True
        elif 1 in self.__plateau[self.getNbLigne() - 1] and (j==-1):
            return True
        for l in range(self.getNbLigne()):
            for c in range(self.getNbColone()):
                if self.__plateau[l][c] == j:
                    if self.isPionMovable(l, c, j):
                        return False

        return True
