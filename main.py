import GlobalValue as Gv
import Computeur as Co

'''
     -- La valeur d’une configuration est l’entier +k si le joueur dont c’est le
    tour peut gagner en maximum k coups, quoi que fasse l’autre.
     -- La valeur est l’entier −k si le joueur dont c’est le tour ne peut pas gagner mais qu’il
    peut survivre au moins k coups face à un adversaire optimal
     -- La valeur est 0 si le joueur dont c’est le tour a perdu
     -- L'ia joue toujour les noires
'''

G = Gv.GlobalValue()

# Test du getPlateauWidthConfigLoaded
plateau = G.getPlateauWithConfigLoaded()
computeur = Co.Computeur(plateau)
print(plateau.toString())
print(computeur.algo(plateau))


# Test getMeilleurCoup et getValeurPredecesseur
if False:  # Active ou desactive les testes
    print(computeur.getMeilleurCoup([1, 2, 3]))  # print 3
    print(computeur.getValeurPredecesseur([1, 2, 3]))  # print 4
    print(computeur.getValeurPredecesseur([1, -2, 3]))  # print -1
    print(computeur.getValeurPredecesseur([-1, -2, -3]))  # print 0
    print(computeur.getMeilleurCoup([1, 1, 3, 3])) # print 1
    print(computeur.getMeilleurCoup([-4, -6])) # print -6
    print(computeur.getMeilleurCoup([-4, 1])) # print 1

if False:# 3x4_1
    print(plateau.aPerdu(-1))#False
    print(plateau.aPerdu(1))#False

    print(plateau.pionAvance(1, 2, -1))
    print(plateau.toString())

    print(plateau.aPerdu(-1))  #False
    print(plateau.aPerdu(1))  #True

if False:# 3x4_1
    print(plateau.pionAvance(1,1,-1))#False
    print(plateau.pionAvance(1,0,1))#False
    print(plateau.toString())
    pass

if False: # 3x4_1
    print(plateau.canPionTake(1,1,-1,'g'))#False
    print(plateau.canPionTake(1,1,-1,'d'))#False

    print(plateau.canPionTake(1,2,-1,'g'))#True
    print(plateau.canPionTake(1,2,-1,'d'))#True

    print(plateau.canPionTake(0,1,1,'g'))#False
    print(plateau.canPionTake(0,1,1,'d'))#True

    #print(plateau.aPerdu(-1))

    plateau.pionAvance(2,3,-1)
    print(plateau.toString())

    print(plateau.isPionMovable(0,1,1))#True

    plateau.pionTake(1,2,-1,'d')
    print(plateau.toString())

    print(plateau.isPionMovable(0,1,1))#False
    print(plateau.isPionMovable(1,0,1))#False

if False :
    print(plateau.canPionAvance(1, 1, -1))
    plateau.pionAvance(1, 1, -1)
    print(plateau.toString())


# Test hashId
if False:
    print(computeur.hashID(plateau))
