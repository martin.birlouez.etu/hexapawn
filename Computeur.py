import Plateau
from Plateau import Plateau
import copy


class Computeur:
    __configCalculer: {} = {}  # dictionnaire (hash map) qui permet de sauvgader les configuartion caluler

    # __pions: [[(int, int)], [(int, int)]] = [[], []]  # [pions blanc (l,c)] [pions noir (l,c)]

    def __init__(self, plateau):
        # self.__configCalculer[self.hashID(plateau)] = self.getValuation(plateau)
        # Coor des pion blanc (l,c)
        # for k in range(0, plateau.getNbColone()):
        #    self.__pions[0].append((0, k))
        #    self.__pions[1].append((plateau.getNbColone() - 1, k))
        # print(self.__pions)
        pass

    def getMeilleurCoup(self, listValuation: [int]) -> int:
        r = listValuation[0]
        for v in listValuation:
            if v < 0:
                if r < 0:
                    r = max(r, v)
                else:
                    r = v
            elif v > 0 and r > 0:
                r = max(v, r)
            elif v == 0:
                return 0
        return r

    def getValeurPredecesseur(self, listValuationCoup) -> int:
        return self.getMeilleurCoup(listValuationCoup) + 1

    # Fonction de hachage de configuration d'un plateau
    def hashID(self, plateau: Plateau) -> int:
        r = 0
        for l in range(plateau.getNbLigne()):
            for c in range(plateau.getNbColone()):
                r += plateau.getValue(l, c) * plateau.getNbColone() + c + 1
        return r

    # Fonction qui renvoie la valeur d'une configuration
    def getValuation(self, plateau: Plateau) -> int:
        return self.algo(plateau)

    # Fonction qui renvoie si les blanc joue (permet de savoir qui doit jouer)
    def blancJoue(self, plateau: Plateau) -> bool:
        pass

    def devAlgo(self, plateau: Plateau, tour: int, joueurActu: int, l: int, c: int) -> int:
        listVal: [int] = []

        platSave = plateau.getCopy()
        if plateau.pionAvance(l, c, joueurActu):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        platSave = plateau.getCopy()
        if plateau.pionTake(l, c, joueurActu, 'd'):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        platSave = plateau.getCopy()
        if plateau.pionTake(l, c, joueurActu, 'g'):
            print(plateau.toString() + "tour :" + str(tour))
            listVal.append(self.algo(plateau, tour))
            plateau.setPlateau(platSave)

        return self.getMeilleurCoup(listVal)

    def algo(self, plateau: Plateau, tour: int = 0) -> int:
        joueurActu = (tour % 2) - 1 if tour % 2 == 0 else (tour % 2)
        listVal: [int] = []
        # -1 si 1 | 1 si 0 pour les methodes de jouabilités de Plateau
        if (plateau.aPerdu(joueurActu)):
            r = tour
            if (tour % 2 == 0):
                r = tour * -1
            return r
        else:
            tour += 1
            for l in range(plateau.getNbLigne()):
                for c in range(plateau.getNbColone()):
                    if plateau.getPlateau()[l][c] == joueurActu and plateau.isPionMovable(l, c, joueurActu):
                        listVal.append(self.devAlgo(plateau, tour, joueurActu, l, c))
            return self.getMeilleurCoup(listVal)
