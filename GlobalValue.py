import Plateau as p


class GlobalValue:
    __nbLignes: int = None  # nombre de lignes
    __nbColones: int = None  # nombre de colones
    __configueLoad: [str] = []  # les donné d'entré du plateau

    def __init__(self):
        self.__nbLignes = int(input())
        self.__nbColones = int(input())
        for i in range(self.__nbLignes):
            lt = input()
            i = len(lt)
            while i < self.__nbColones:
                lt = lt + ' '
                i += 1
            self.__configueLoad.append(lt)

    def getNbLignes(self) -> int:
        return self.__nbLignes

    def getNbColones(self) -> int:
        return self.__nbColones

    def getPlateauWithConfigLoaded(self) -> p.Plateau:
        plt = p.Plateau(self.__nbLignes, self.__nbColones)
        for l in range(self.__nbLignes):
            for c in range(self.__nbColones):
                plt.setValue(l, c, self.__identyfyValueIdx(self.__configueLoad[l][c]))
        return plt

    # Le plateau : 0  représente la case vide 1 le pion blanc et -1 le pion noir
    def __identyfyValueIdx(self, v: str) -> int:
        if v == 'p':
            return 1
        elif v == 'P':
            return -1
        else:
            return 0
